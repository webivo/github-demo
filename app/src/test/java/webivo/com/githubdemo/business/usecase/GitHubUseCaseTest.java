package webivo.com.githubdemo.business.usecase;

import android.support.annotation.NonNull;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.internal.schedulers.ExecutorScheduler;
import io.reactivex.plugins.RxJavaPlugins;
import webivo.com.githubdemo.business.repository.GitHubRepository;
import webivo.com.githubdemo.network.model.Repository;
import webivo.com.githubdemo.network.rest.GitHubRetrofit;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GitHubUseCaseTest {

    @Mock GitHubRetrofit mGitHubRetrofit;
    @Mock GitHubRepository mGitHubRepository;
    private GitHubUseCase mGitHubUseCase;

    @Before
    public void setUp() {
        mGitHubUseCase = new GitHubUseCase(mGitHubRetrofit,
                mGitHubRepository);
    }

    @BeforeClass
    public static void setUpRxSchedulers() {
        final Scheduler immediate = new Scheduler() {
            @Override
            public Disposable scheduleDirect(@NonNull Runnable run, long delay, @NonNull TimeUnit unit) {
                return super.scheduleDirect(run, 0, unit);
            }

            @Override
            public Worker createWorker() {
                return new ExecutorScheduler.ExecutorWorker(new Executor() {
                    @Override
                    public void execute(@NonNull Runnable command) {
                        command.run();
                    }
                });
            }
        };

        RxJavaPlugins.setInitIoSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(Callable<Scheduler> schedulerCallable) throws Exception {
                return immediate;
            }
        });
        RxJavaPlugins.setInitComputationSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(Callable<Scheduler> schedulerCallable) throws Exception {
                return immediate;
            }
        });
        RxJavaPlugins.setInitNewThreadSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(Callable<Scheduler> schedulerCallable) throws Exception {
                return immediate;
            }
        });
        RxJavaPlugins.setInitSingleSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(Callable<Scheduler> schedulerCallable) throws Exception {
                return immediate;
            }
        });
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(Callable<Scheduler> schedulerCallable) throws Exception {
                return immediate;
            }
        });
    }

    @Test
    public void getAllRepositories_givenRequestIsSuccessful_returnAllRepositories() {
        final List<Repository> repositoryList = new ArrayList<>();
        when(mGitHubRetrofit.getAllRepositories()).thenReturn(Single.just(repositoryList));

        mGitHubUseCase.getAllRepositories()
                .test()
                .assertNoErrors()
                .assertComplete();
    }

    @Test
    public void getAllRepositories_givenRequestIsNotSuccesful_doNotReturnAllRepositories() {
        final Throwable throwable = new Throwable();

        when(mGitHubRetrofit.getAllRepositories()).thenReturn(Single.<List<Repository>>error(throwable));

        mGitHubUseCase.getAllRepositories()
                .test()
                .assertError(throwable)
                .assertNotComplete();
    }
}
