package webivo.com.githubdemo.ui.allrepositories;

import android.support.annotation.NonNull;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.internal.schedulers.ExecutorScheduler;
import io.reactivex.plugins.RxJavaPlugins;
import webivo.com.githubdemo.business.model.RepositoryDomainModel;
import webivo.com.githubdemo.business.repository.GitHubRepository;
import webivo.com.githubdemo.business.usecase.GitHubUseCase;
import webivo.com.githubdemo.business.usecase.GitHubUseCaseTest;
import webivo.com.githubdemo.network.model.Owner;
import webivo.com.githubdemo.network.model.Repository;
import webivo.com.githubdemo.network.rest.GitHubRetrofit;
import webivo.com.githubdemo.ui.model.RepositoryModel;

import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AllRepositoriesPresenterTest {

    @Mock GitHubUseCase mGitHubUseCase;
    @Mock GitHubRepository mGitHubRepository;
    @Mock AllRepositoriesContract.View mView;
    private AllRepositoriesPresenter mPresenter;

    @Before
    public void setUp() {
        mPresenter = new AllRepositoriesPresenter(mGitHubUseCase,
                mGitHubRepository);
        mPresenter.attachView(mView);
    }

    @BeforeClass
    public static void setUpRxSchedulers() {
        final Scheduler immediate = new Scheduler() {
            @Override
            public Disposable scheduleDirect(@NonNull Runnable run, long delay, @NonNull TimeUnit unit) {
                // this prevents StackOverflowErrors when scheduling with a delay
                return super.scheduleDirect(run, 0, unit);
            }

            @Override
            public Worker createWorker() {
                return new ExecutorScheduler.ExecutorWorker(new Executor() {
                    @Override
                    public void execute(@NonNull Runnable command) {
                        command.run();
                    }
                });
            }
        };

        RxJavaPlugins.setInitIoSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(Callable<Scheduler> schedulerCallable) throws Exception {
                return immediate;
            }
        });
        RxJavaPlugins.setInitComputationSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(Callable<Scheduler> schedulerCallable) throws Exception {
                return immediate;
            }
        });
        RxJavaPlugins.setInitNewThreadSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(Callable<Scheduler> schedulerCallable) throws Exception {
                return immediate;
            }
        });
        RxJavaPlugins.setInitSingleSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(Callable<Scheduler> schedulerCallable) throws Exception {
                return immediate;
            }
        });
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(Callable<Scheduler> schedulerCallable) throws Exception {
                return immediate;
            }
        });
    }

    @Test
    public void getAllRepositories_givenRequestSuccess_returnAllRepositoriesList() {
        final Repository repository = new Repository();
        final Owner owner = new Owner();
        owner.setAvatarUrl("https://api.github.com/avatarUrl");
        owner.setLogin("dragosivanov");
        owner.setFollowersUrl("https://api.github.com/followersUrl");
        repository.setDescription("Any Description");
        repository.setFullName("Dragos Ivanov");
        repository.setName("dragosivanov");
        repository.setId(1);
        repository.setOwner(owner);
        final RepositoryDomainModel repositoryDetailsDomainModel = new RepositoryDomainModel(repository);
        final List<RepositoryDomainModel> repositoryDomainModelList = new ArrayList<>();
        repositoryDomainModelList.add(repositoryDetailsDomainModel);

        when(mGitHubUseCase.shouldGetAllRepositories()).thenReturn(true);
        when(mGitHubUseCase.getAllRepositories()).thenReturn(Single.just(repositoryDomainModelList));

        mPresenter.getAllRepositories();

        verify(mView).showAllRepositories(anyList());
    }

    @Test
    public void getAllRepositories_givenRequestFailure_doNotReturnAllRepositoriesList() {
        when(mGitHubUseCase.shouldGetAllRepositories()).thenReturn(true);
        when(mGitHubUseCase.getAllRepositories()).thenReturn(Single.<List<RepositoryDomainModel>>error(new Throwable()));

        mPresenter.getAllRepositories();

        verify(mView, never()).showAllRepositories(anyList());
    }

    @Test
    public void openRepositoryDetailsActivity_showRepositoryDetailsActivity() {
        mPresenter.openRepositoryDetailsActivity(anyString());

        verify(mView).showRepositoryDetailsActivity(anyString());
    }
}
