package webivo.com.githubdemo.ui.repositorydetails;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Single;
import webivo.com.githubdemo.business.model.RepositoryDetailsDomainModel;
import webivo.com.githubdemo.business.usecase.GitHubUseCase;
import webivo.com.githubdemo.business.usecase.GitHubUseCaseTest;
import webivo.com.githubdemo.network.model.Owner;
import webivo.com.githubdemo.network.model.RepositoryDetails;
import webivo.com.githubdemo.ui.model.RepositoryDetailsModel;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RepositoryDetailsPresenterTest {

    @Mock RepositoryDetailsContract.View mView;
    @Mock GitHubUseCase mGitHubUseCase;
    private RepositoryDetailsPresenter mPresenter;

    @Before
    public void setUp() {
        mPresenter = new RepositoryDetailsPresenter(mGitHubUseCase);
        mPresenter.attachView(mView);
    }
}
