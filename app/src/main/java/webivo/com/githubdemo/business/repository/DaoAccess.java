package webivo.com.githubdemo.business.repository;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import webivo.com.githubdemo.network.model.Repository;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface DaoAccess {

    @Insert(onConflict = REPLACE)
    void addAllRepositories(List<Repository> repositoryList);

    @Query("SELECT * FROM repository")
    Flowable<List<Repository>> getAllCachedRepositories();

    @Query("DELETE FROM repository")
    public void deleteAllRepositories();
}
