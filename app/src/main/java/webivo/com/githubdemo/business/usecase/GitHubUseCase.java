package webivo.com.githubdemo.business.usecase;


import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import webivo.com.githubdemo.business.model.RepositoryDetailsDomainModel;
import webivo.com.githubdemo.business.model.RepositoryDomainModel;
import webivo.com.githubdemo.business.repository.GitHubRepository;
import webivo.com.githubdemo.network.model.Repository;
import webivo.com.githubdemo.network.model.RepositoryDetails;
import webivo.com.githubdemo.network.rest.GitHubRetrofit;

public class GitHubUseCase {

    private final GitHubRetrofit mGitHubRetrofit;
    private final GitHubRepository mGitHUbRepository;

    @Inject
    public GitHubUseCase(GitHubRetrofit gitHubRetrofit,
                         GitHubRepository gitHubRepository) {
        mGitHubRetrofit = gitHubRetrofit;
        mGitHUbRepository = gitHubRepository;
    }

    public Single<List<RepositoryDomainModel>> getAllRepositories() {
        return mGitHubRetrofit.getAllRepositories()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterSuccess(new Consumer<List<Repository>>() {
                    @Override
                    public void accept(List<Repository> repositoryList) throws Exception {
                        mGitHUbRepository.cacheAllLocalRepositories(repositoryList);
                    }
                })
                .map(new Function<List<Repository>, List<RepositoryDomainModel>>() {
                    @Override
                    public List<RepositoryDomainModel> apply(final List<Repository> repositories) throws Exception {
                        return RepositoryDomainModel.fromAllRepositories(repositories);
                    }
                });
    }

    public Single<RepositoryDetailsDomainModel> getRepositoryDetails(String repositoryFullName) {
        return mGitHubRetrofit.getRepositoryDetails(repositoryFullName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Function<RepositoryDetails, RepositoryDetailsDomainModel>() {
                    @Override
                    public RepositoryDetailsDomainModel apply(RepositoryDetails repositoryDetails)
                            throws Exception {
                        return RepositoryDetailsDomainModel.fromRepositoryDetails(repositoryDetails);
                    }
                });
    }

    public boolean shouldGetAllRepositories() {
        return Math.abs(mGitHUbRepository.getSavedTime() - System.currentTimeMillis()) > 1800000;
    }
}
