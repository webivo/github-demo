package webivo.com.githubdemo.business.repository;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import webivo.com.githubdemo.network.model.Owner;
import webivo.com.githubdemo.network.model.Repository;

@Database(entities = {Repository.class, Owner.class}, version = 1, exportSchema = false)
public abstract class RepositoryDatabase extends RoomDatabase{
    public abstract DaoAccess daoAccess();
}
