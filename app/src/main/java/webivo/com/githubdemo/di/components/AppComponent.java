package webivo.com.githubdemo.di.components;

import dagger.Component;
import webivo.com.githubdemo.App;
import webivo.com.githubdemo.di.modules.ActivityModule;
import webivo.com.githubdemo.di.modules.AppModule;
import webivo.com.githubdemo.di.scopes.ApplicationScope;

@ApplicationScope
@Component(modules = {AppModule.class})
public interface AppComponent {
    void inject(App mainApplication);

    ActivityComponent newActivityComponent(ActivityModule activityModule);
}
