package webivo.com.githubdemo.di.components;

import dagger.Component;
import dagger.Subcomponent;
import webivo.com.githubdemo.di.modules.ActivityModule;
import webivo.com.githubdemo.di.scopes.ActivityScope;
import webivo.com.githubdemo.ui.allrepositories.AllRepositoriesActivity;
import webivo.com.githubdemo.ui.repositorydetails.RepositoryDetailsActivity;

@ActivityScope
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {
    void inject(AllRepositoriesActivity allRepositoriesActivity);

    void inject(RepositoryDetailsActivity repositoryDetailsActivity);
}
