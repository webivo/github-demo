package webivo.com.githubdemo.di.modules;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import dagger.Module;
import dagger.Provides;
import webivo.com.githubdemo.di.scopes.ActivityScope;
import webivo.com.githubdemo.ui.allrepositories.AllRepositoriesAdapter;

@Module
public class ActivityModule {

    private final Activity mActivity;

    public ActivityModule(Activity activity) {
        mActivity = activity;
    }

    @Provides
    @ActivityScope
    Activity getActivity() {
        return mActivity;
    }

    @Provides
    @ActivityScope
    AllRepositoriesAdapter getAllRepositoriesAdapter() {
        return new AllRepositoriesAdapter();
    }

    @Provides
    @ActivityScope
    SharedPreferences getAppPreferences() {
        return mActivity.getSharedPreferences("GitHubSharedPrefs", Context.MODE_PRIVATE);
    }
}
