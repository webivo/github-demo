package webivo.com.githubdemo.ui.repositorydetails;

import webivo.com.githubdemo.ui.MvpPresenter;
import webivo.com.githubdemo.ui.MvpView;
import webivo.com.githubdemo.ui.model.RepositoryDetailsModel;

public class RepositoryDetailsContract {

    interface View extends MvpView {
        void showRepositoryDetails(RepositoryDetailsModel repositoryDetailsModel);
    }

    interface RepositoryDetailsActionListener<T extends MvpView> extends MvpPresenter<T> {
        void getRepositoryDetails(String repositoryFullName);
    }
}
