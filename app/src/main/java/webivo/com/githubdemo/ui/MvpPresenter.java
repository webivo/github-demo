package webivo.com.githubdemo.ui;

public interface MvpPresenter<T extends MvpView> {

    void attachView(T view);

    void detachView();
}
