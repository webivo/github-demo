package webivo.com.githubdemo.ui.allrepositories;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import webivo.com.githubdemo.R;
import webivo.com.githubdemo.ui.BaseActivity;
import webivo.com.githubdemo.ui.model.RepositoryModel;
import webivo.com.githubdemo.ui.repositorydetails.RepositoryDetailsActivity;
import webivo.com.githubdemo.utils.Constants;

public class AllRepositoriesActivity extends BaseActivity
        implements AllRepositoriesContract.View, AllRepositoriesListener {

    @BindView(R.id.allRepositoriesRecyclerView) RecyclerView allRepositoriesRecyclerView;
    @Inject protected AllRepositoriesPresenter mAllRepositoriesPresenter;
    @Inject protected AllRepositoriesAdapter mAllRepositoriesAdapter;
    private AllRepositoriesListener mAllRepositoriesListener;
    private List<RepositoryModel> allRepositoriesList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        getControllerComponent().inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_repositories);
        ButterKnife.bind(this);
        mAllRepositoriesPresenter.attachView(this);
        mAllRepositoriesListener = this;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAllRepositoriesPresenter.getAllRepositories();
    }

    @Override
    public void showAllRepositories(List<RepositoryModel> repositoryModelList) {
        allRepositoriesRecyclerView.setHasFixedSize(true);
        allRepositoriesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAllRepositoriesAdapter.setAllRepositories(repositoryModelList);
        allRepositoriesRecyclerView.setAdapter(mAllRepositoriesAdapter);
        mAllRepositoriesAdapter.setOnItemClickListener(mAllRepositoriesListener);
        allRepositoriesList = repositoryModelList;
    }

    @Override
    public void showRepositoryDetailsActivity(String repositoryName) {
        final Intent intent = new Intent(this, RepositoryDetailsActivity.class);
        intent.putExtra(Constants.REPOSITORY_NAME, repositoryName);
        startActivity(intent);
    }

    @Override
    public void onItemClickListener(int position) {
        mAllRepositoriesPresenter.openRepositoryDetailsActivity(allRepositoriesList.get(position)
                .getRepositoryFullName());
    }

    @Override
    protected void onDestroy() {
        mAllRepositoriesPresenter.detachView();
        super.onDestroy();
    }
}
