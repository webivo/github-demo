package webivo.com.githubdemo.ui.allrepositories;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import webivo.com.githubdemo.R;
import webivo.com.githubdemo.ui.model.RepositoryModel;

public class AllRepositoriesAdapter extends RecyclerView.Adapter<AllRepositoriesViewHolder> {

    private final static int MAX_LIST_SIZE = 25;
    private List<RepositoryModel> mAllRepositoriesList;
    private AllRepositoriesListener mAllRepositoriesListener;

    public AllRepositoriesAdapter() {
        mAllRepositoriesList = new ArrayList<>();
    }

    public void setAllRepositories(List<RepositoryModel> repositoryModelList) {
        mAllRepositoriesList = repositoryModelList;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(AllRepositoriesListener allRepositoriesListener) {
        mAllRepositoriesListener = allRepositoriesListener;
    }

    @NonNull
    @Override
    public AllRepositoriesViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new AllRepositoriesViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.view_repository_details_item, viewGroup, false),
                mAllRepositoriesListener);
    }

    @Override
    public void onBindViewHolder(@NonNull AllRepositoriesViewHolder allRepositoriesViewHolder, int itemPosition) {
        allRepositoriesViewHolder.bindItems(mAllRepositoriesList);
    }

    @Override
    public int getItemCount() {
        return MAX_LIST_SIZE;
    }
}
