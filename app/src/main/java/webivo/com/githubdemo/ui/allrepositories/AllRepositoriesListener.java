package webivo.com.githubdemo.ui.allrepositories;

public interface AllRepositoriesListener {
    void onItemClickListener(int itemPosition);
}
