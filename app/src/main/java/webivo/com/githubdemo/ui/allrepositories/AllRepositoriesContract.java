package webivo.com.githubdemo.ui.allrepositories;

import java.util.List;

import webivo.com.githubdemo.ui.MvpPresenter;
import webivo.com.githubdemo.ui.MvpView;
import webivo.com.githubdemo.ui.model.RepositoryModel;

public interface AllRepositoriesContract {
    interface View extends MvpView{
        void showAllRepositories(List<RepositoryModel> repositoryModelList);

        void showRepositoryDetailsActivity(String repositoryName);
    }

    interface AllRepositoriesActionListener<T extends MvpView> extends MvpPresenter<T>{
        void getAllRepositories();

        void openRepositoryDetailsActivity(String repositoryName);
    }
}
