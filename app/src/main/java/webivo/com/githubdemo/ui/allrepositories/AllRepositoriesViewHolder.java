package webivo.com.githubdemo.ui.allrepositories;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import webivo.com.githubdemo.R;
import webivo.com.githubdemo.di.modules.GlideApp;
import webivo.com.githubdemo.ui.model.RepositoryModel;

public class AllRepositoriesViewHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener {

    @BindView(R.id.repositoryCardView) CardView mRepositoryCardView;
    @BindView(R.id.authorImageView) ImageView mAuthorImageView;
    @BindView(R.id.titleTextView) TextView mTitleTextView;
    @BindView(R.id.repositoryNameTextView) TextView mRepositoryName;
    @BindView(R.id.descriptionTextView) TextView mDescriptionTextView;

    private final AllRepositoriesListener mAllRepositoriesListener;

    public AllRepositoriesViewHolder(@NonNull View itemView,
                                     AllRepositoriesListener allRepositoriesListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        mAllRepositoriesListener = allRepositoriesListener;
        mRepositoryCardView.setOnClickListener(this);
    }

    public void bindItems(List<RepositoryModel> repositoryModelList) {
        final RepositoryModel repositoryModel = repositoryModelList.get(getAdapterPosition());
        GlideApp.with(itemView.getContext())
                .load(repositoryModel.getAuthorImageUrl())
                .into(mAuthorImageView);
        mRepositoryName.setText(repositoryModel.getRepositoryName());
        mTitleTextView.setText(repositoryModel.getAuthorName());
        mDescriptionTextView.setText(splitDescription(repositoryModel.getRepositoryDescription()));
    }

    private String splitDescription(String description) {
        if (description != null && description.split("\\s+").length > 15) {
            final StringBuilder builder = new StringBuilder();
            for (String word : description.split("\\s+")) {
                if(builder.toString().split("\\s+").length < 15) {
                    builder.append(word).append(" ");
                }
            }
            return builder.toString().replaceAll("\\s+$", "") + "...";
        }
        return description;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == mRepositoryCardView.getId()) {
            mAllRepositoriesListener.onItemClickListener(getAdapterPosition());
        }
    }
}
