package webivo.com.githubdemo.ui;

import android.support.v7.app.AppCompatActivity;

import webivo.com.githubdemo.App;
import webivo.com.githubdemo.di.components.ActivityComponent;
import webivo.com.githubdemo.di.modules.ActivityModule;

/**
 * This class at this moment is not mandatory to be abstract, but for future work we will leave it
 * like this
 */
public abstract class BaseActivity extends AppCompatActivity {
    protected ActivityComponent getControllerComponent() {
        return ((App) getApplication()).getApplicationComponent()
                .newActivityComponent(new ActivityModule(this));
    }
}
