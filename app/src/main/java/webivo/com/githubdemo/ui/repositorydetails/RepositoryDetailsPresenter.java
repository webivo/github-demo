package webivo.com.githubdemo.ui.repositorydetails;

import android.util.Log;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import webivo.com.githubdemo.business.model.RepositoryDetailsDomainModel;
import webivo.com.githubdemo.business.usecase.GitHubUseCase;
import webivo.com.githubdemo.ui.model.RepositoryDetailsModel;

public class RepositoryDetailsPresenter
        implements RepositoryDetailsContract.RepositoryDetailsActionListener<RepositoryDetailsContract.View> {
    private static String TAG = "RepositoryDetailsPresenter";
    private RepositoryDetailsContract.View mView;
    private final GitHubUseCase mGitHubUseCase;
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    @Inject
    public RepositoryDetailsPresenter(GitHubUseCase gitHubUseCase) {
        mGitHubUseCase = gitHubUseCase;
    }

    @Override
    public void attachView(RepositoryDetailsContract.View view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mView = null;
        mCompositeDisposable.clear();
    }

    @Override
    public void getRepositoryDetails(String repositoryFullName) {
        Disposable disposable = mGitHubUseCase.getRepositoryDetails(repositoryFullName)
                .subscribe(new Consumer<RepositoryDetailsDomainModel>() {
                    @Override
                    public void accept(RepositoryDetailsDomainModel repositoryDetailsDomainModel)
                            throws Exception {
                        mView.showRepositoryDetails(new RepositoryDetailsModel(repositoryDetailsDomainModel));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.e(TAG, throwable.getMessage());
                    }
                });
        mCompositeDisposable.add(disposable);
    }
}
