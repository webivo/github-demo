package webivo.com.githubdemo.ui.allrepositories;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import webivo.com.githubdemo.business.model.RepositoryDomainModel;
import webivo.com.githubdemo.business.repository.GitHubRepository;
import webivo.com.githubdemo.business.usecase.GitHubUseCase;
import webivo.com.githubdemo.ui.model.RepositoryModel;

public class AllRepositoriesPresenter implements
        AllRepositoriesContract.AllRepositoriesActionListener<AllRepositoriesContract.View> {

    private AllRepositoriesContract.View mView;
    private final GitHubUseCase mGitHubUseCase;
    private final GitHubRepository mGitHubRepository;
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    @Inject
    public AllRepositoriesPresenter(GitHubUseCase gitHubUseCase,
                                    GitHubRepository gitHubRepository) {
        mGitHubUseCase = gitHubUseCase;
        mGitHubRepository = gitHubRepository;
    }

    @Override
    public void attachView(AllRepositoriesContract.View view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mView = null;
        mCompositeDisposable.clear();
    }

    @Override
    public void getAllRepositories() {
        final boolean shouldgetAllRepositories = mGitHubUseCase.shouldGetAllRepositories();
        final Disposable disposable;
        if (shouldgetAllRepositories) {
            disposable = getAllGitHubRepositories();
        } else {
            disposable = getAllCachedRepositories();
        }
        mCompositeDisposable.add(disposable);
    }

    private Disposable getAllGitHubRepositories() {
        return mGitHubUseCase.getAllRepositories()
                .subscribe(new Consumer<List<RepositoryDomainModel>>() {
                    @Override
                    public void accept(List<RepositoryDomainModel> repositoryDomainModelList)
                            throws Exception {
                        mGitHubRepository.saveCurrentTime(System.currentTimeMillis());
                        mView.showAllRepositories(fromRepositoryDomainModel(repositoryDomainModelList));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //Should create a custom Logger to be able to test it
                    }
                });
    }

    private Disposable getAllCachedRepositories() {
        return mGitHubRepository.getAllCachedRepositories()
                .subscribe(new Consumer<List<RepositoryDomainModel>>() {
                    @Override
                    public void accept(List<RepositoryDomainModel> repositoryDomainModelList)
                            throws Exception {
                        mView.showAllRepositories(fromRepositoryDomainModel(repositoryDomainModelList));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                    }
                });
    }

    @Override
    public void openRepositoryDetailsActivity(String repositoryName) {
        mView.showRepositoryDetailsActivity(repositoryName);
    }

    private List<RepositoryModel> fromRepositoryDomainModel(List<RepositoryDomainModel> repositoryDomainModelList) {
        final List<RepositoryModel> repositoryModelList = new ArrayList<>();
        for (RepositoryDomainModel repositoryDomainModel : repositoryDomainModelList) {
            repositoryModelList.add(new RepositoryModel(repositoryDomainModel));
        }
        return repositoryModelList;
    }
}
