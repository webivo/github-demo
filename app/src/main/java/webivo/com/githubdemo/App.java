package webivo.com.githubdemo;

import android.app.Application;

import com.facebook.stetho.Stetho;

import webivo.com.githubdemo.di.components.AppComponent;
import webivo.com.githubdemo.di.components.DaggerAppComponent;
import webivo.com.githubdemo.di.modules.AppModule;


public class App extends Application {

    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        getApplicationComponent().inject(this);
        super.onCreate();
        //Just For Debug Purposes. We can make logic to show it only in DEBUG builds.
        Stetho.initializeWithDefaults(this);
    }

    public AppComponent getApplicationComponent() {
        if (mAppComponent == null) {
            mAppComponent = DaggerAppComponent.builder()
                    .appModule(new AppModule(this))
                    .build();
        }
        return mAppComponent;
    }
}
