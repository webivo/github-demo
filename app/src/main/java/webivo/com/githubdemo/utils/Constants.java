package webivo.com.githubdemo.utils;

import android.support.annotation.StringDef;

@StringDef({Constants.REPOSITORY_NAME})
public @interface Constants {
    String REPOSITORY_NAME = "Repository Name";
}
