package webivo.com.githubdemo.utils;

import android.arch.persistence.room.TypeConverter;

import webivo.com.githubdemo.network.model.Owner;

public class OwnerTypeConverter {

    @TypeConverter
    public static String fromOwner(Owner owner) {
        if (owner == null) {
            return (null);
        }
        return (String.format("%s,%s,%s", owner.getAvatarUrl(), owner.getFollowersUrl(), owner.getLogin()));
    }

    @TypeConverter
    public static Owner toOwner(String owner) {
        if (owner == null) {
            return (null);
        }
        String[] pieces = owner.split(",");
        Owner result = new Owner();
        result.setAvatarUrl(pieces[0]);
        result.setFollowersUrl(pieces[1]);
        result.setLogin(pieces[2]);
        return (result);
    }
}
