package webivo.com.githubdemo.utils;

import android.arch.persistence.room.TypeConverter;

public class RoomConverterNullString {
    @TypeConverter
    public static String fromNullToString(String value) {
        if (value == null) {
            return "";
        } else {
            return value;
        }
    }
}
