package webivo.com.githubdemo.network.rest;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import webivo.com.githubdemo.network.model.Repository;
import webivo.com.githubdemo.network.model.RepositoryDetails;

public interface GitHubRetrofit {
    @GET("repositories")
    Single<List<Repository>> getAllRepositories();

    @GET("repos/{repositoryFullName}")
    Single<RepositoryDetails> getRepositoryDetails(@Path(value = "repositoryFullName", encoded = true)
                                                           String repositoryFullName);
}
